FROM node:16

WORKDIR /usr/app

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build
# RUN cp ./.env.production ./build/.env
# RUN node build/server.js

EXPOSE 3042

CMD ["npm", "run", "start:prod"]
