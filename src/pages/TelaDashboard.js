import { useEffect, useState, useRef } from 'react';
import { useHistory } from "react-router-dom";
// import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
// import { useApi } from '../hooks/useApi'
import { useDispatch, useSelector } from 'react-redux';
import {
  BsFillFilePersonFill,
  BsFillAlarmFill,
} from "react-icons/bs";
import TelaDashboard from '../assets/css/TelaDashboard.css'
// import GraficoSetores from '../components/GraficoSetores';
// import GraficoPizza from '../components/GraficoPizza';



import {
  Alert,
  Table,
  Button,
  Form,
  FormControl,
  Container,
  Row,
  Col,
  InputGroup,
  Card,
  CardGroup,
} from "react-bootstrap";

import peopleActions from '../redux/actions/peopleActions';


const Cadastro = (props) => {
  // const classes = useStyles();
  // const api = useApi()




  return (
    <div className="root" >

      <Alert
        variant="secondary"
        style={{ width: "100%", display: 'flex' }}
        className="mb-4"
      >
        <Alert.Heading className="text-center m-auto" style={{ color: "#000" }}>
          Dashboard
        </Alert.Heading>
      </Alert>

      <Container>

        <Row>
          <Col>
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <BsFillFilePersonFill className="nc-icon nc-globe text-warning" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Vagas Disponível</p>
                      <Card.Title tag="p">25</Card.Title>
                      <p />
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr />
                <div className="stats">
                </div>
              </Card.Footer>
            </Card>
          </Col>

          <Col>
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <BsFillFilePersonFill className="nc-icon nc-money-coins text-success" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Estagiários Ativos</p>
                      <Card.Title tag="p">89</Card.Title>
                      <p />
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr />
                <div className="stats">
                  {/* <i className="far fa-calendar" /> Last day */}
                </div>
              </Card.Footer>
            </Card>
          </Col>

          <Col>
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <BsFillFilePersonFill className="nc-icon nc-vector text-danger" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Final do Contrato</p>
                      <Card.Title tag="p">23</Card.Title>
                      <p />
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr />
                <div className="stats">
                </div>
              </Card.Footer>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <BsFillFilePersonFill className="nc-icon nc-favourite-28 text-primary" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Inativos</p>
                      <Card.Title tag="p">97</Card.Title>
                      <p />
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr />
                <div className="stats">
                </div>
              </Card.Footer>
            </Card>
          </Col>
        </Row>

        {/* INICIO DO GRAFICO SETORES */}

        <Row className='GraficoSetore'>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title tag="h5">Estagiários por Setor</Card.Title>
              </Card.Header>

              <Card.Body >

                  {/* <GraficoSetores /> */}

              </Card.Body>

            </Card>
          </Col>
        </Row>
        {/* FINAL DO GRAFICO SETORES */}

        {/* INICIO GRAFICO PIZZA */}


        <Card className="GraficoPizza">
          <Row className="m-0 p-1">
            <Col xs="12" sm="4" md="4" lg="4" xl="4">
                <Card.Header>
                  <Card.Title tag="h5">Relação dos Contratos</Card.Title>
                </Card.Header>
                <Card.Body>
                    {/* <GraficoPizza  /> */}
                </Card.Body>
            </Col>
            <Col xs="12" sm="4" md="4" lg="4" xl="4">
                <Card.Header>
                  <Card.Title tag="h5">Relação dos Contratos</Card.Title>
                </Card.Header>
                <Card.Body>
                    {/* <GraficoPizza  /> */}
                </Card.Body>
            </Col>
            <Col xs="12" sm="4" md="4" lg="4" xl="4">
                <Card.Header>
                  <Card.Title tag="h5">Relação dos Contratos</Card.Title>
                </Card.Header>
                <Card.Body>
                    {/* <GraficoPizza  /> */}
                </Card.Body>
            </Col>
          </Row>
        </Card>





      </Container>

    </div>
  );
}

export default Cadastro;