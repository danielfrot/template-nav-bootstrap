import { useEffect, useState, useRef } from 'react';
import { useHistory } from "react-router-dom";
// import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
// import { useApi } from '../hooks/useApi'
import { useDispatch, useSelector } from 'react-redux';
import { BsSearch } from "react-icons/bs";


import {
  Alert,
  Table,
  Button,
  Form,
  FormControl,
  Container,
  Row,
  Col,
  InputGroup,
} from "react-bootstrap";

import peopleActions from '../redux/actions/peopleActions';

const Cadastro = (props) => {
  // const classes = useStyles();

  // const api = useApi()



  return (
    <div className="root" >

      <Alert
        variant="secondary"
        style={{ width: "100%", display: 'flex' }}
        className="mb-4"
      >
        <Alert.Heading className="text-center m-auto" style={{ color: "#000" }}>
          Modelo de Tabela
        </Alert.Heading>
      </Alert>



      <Container fluid>

        {/* Input de pesquisa e botão de cadastro de novos usuários */}

        {/* <section style={{ display: 'flex', backgroundColor: '#f1f1f1', justifyContent: 'space-between', flexWrap: 'wrap-reverse' }}> */}
        {/* <section className='d-flex aling-items-center justify-content-between mb-3'>
          <Row>
            <Form>
              <Col xs lg="12">
                <InputGroup className="">
                  <FormControl placeholder="Pesquisa ID, Nome, CPF ou RG" style={{ width: '555px' }} />
                  <Button type="submit" variant="outline-secondary" id="button-addon2">
                    <BsSearch />
                  </Button>
                </InputGroup>
              </Col>
            </Form>
          </Row>

          <Button type="submit" variant="primary">Cadastrar</Button>
        </section> */}

        {/* Inicio da Tabela */}
        <div style={{ textAlign: "center" }}>
          <Row>
            <Table striped bordered hover responsive size="sm">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>CPF</th>
                  <th>Curso</th>
                  <th>Setor</th>
                  <th>Data</th>
                  <th>Status</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>

                </tr>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>

                </tr>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>

                </tr>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>

                </tr>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>

                </tr>
                <tr>
                  <td>Fulano de tal</td>
                  <td>02465771264</td>
                  <td>Sistema de Informação</td>
                  <td>GEINFO</td>
                  <td>19/04/2021 - 19/04/2022</td>
                  <td>Ativo</td>
                </tr>

              </tbody>
            </Table>
          </Row>
        </div>
      </Container>

    </div>
  );
}

export default Cadastro;