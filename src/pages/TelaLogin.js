import { useEffect } from 'react';
import { useFormik } from 'formik';
import {
  Button,
  Form,
} from "react-bootstrap";

import { useAuth } from '../hooks/useAuth'
import { maskCPF } from '../helpers/mask'
import * as yup from 'yup';
import { toast } from 'react-toastify';

const Login = () => {
  const auth = useAuth()
  const formik = useFormik({
    initialValues: {
      cpf: '',
      senha: '',
    },
    validationSchema: yup.object({
      cpf: yup.string().min(14, 'O CPF deve conter 11 caracteres')
    }),
  })

  const fn_entrar = (e) => {
    e.preventDefault()
    if (formik.errors.cpf) {
      toast.error(formik.errors.cpf)
    } else {
      auth.login(formik.values.cpf, formik.values.senha)
    }
  }

  useEffect(() => {
    formik.setFieldValue('cpf', maskCPF(formik.values.cpf))
  }, [formik.values.cpf])

  return (
    <form onSubmit={fn_entrar} autoComplete="on" style={{ display: 'flex', flexDirection: 'column' }}>
      <Form.Group className="mb-3">
        <Form.Label>CPF</Form.Label>
        <Form.Control
          required
          autoFocus
          type="text"
          {...formik.getFieldProps("cpf")}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Senha</Form.Label>
        <Form.Control
          required
          type="password"
          {...formik.getFieldProps("senha")}
        />
      </Form.Group>
      <Button type="submit">Entrar</Button>
    </form>
  );
}

export default Login;