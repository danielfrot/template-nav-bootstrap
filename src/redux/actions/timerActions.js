

export const att_timer = (payload) => {
  return { type: "ATT_TIMER", payload }
};
export const expired_timer = (payload) => {
  return { type: "EXPIRED_TIMER", payload }
};
export const reset_timer = () => {
  return { type: "RESET_TIMER" }
};
export const addFn_timer = (payload) => {
  return { type: "ADD_FN_RESET_TIMER", payload }
};

export default {
  att_timer,
  expired_timer,
  reset_timer,
  addFn_timer,
};