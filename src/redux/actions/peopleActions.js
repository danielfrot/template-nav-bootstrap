

export const add_all = (payload) => {
  return { type: "ADD_ALL_PEOPLE", payload }
};
export const selected = (payload) => {
  return { type: "SELECTED_PERSON", payload }
};
export const add_people_pendencies = (payload) => {
  return { type: "ADD_PEOPLE_PENDENCIES", payload }
};
export const open_modal = (payload) => {
  return { type: "OPEN_MODAL_PERSON", payload }
};
export const edit_mode = (payload) => {
  return { type: "EDIT_MODE_PERSON", payload }
};
// export const foto_modal = (payload) => {
//   return { type: "FOTO_MODAL_PERSON", payload }
// };
export const foto_selected = (payload) => {
  return { type: "FOTO_SELECTED_PERSON", payload }
};

export const loading = (payload) => {
  return { type: "LOADING_PERSON", payload }
};

export default {
  add_all,
  selected,
  open_modal,
  edit_mode,
  loading,
  add_people_pendencies,
  // foto_modal,
  foto_selected,
};