// import { useAuth } from "../hooks/useAuth";
import { 
  // useHistory,
  NavLink } from "react-router-dom";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import './navbar.css';

// import { useSelector } from "react-redux";

export const NavigationBar = ({ min, seg }) => {
  // const auth = useAuth();
  // const history = useHistory();
  // const authReducer = useSelector((state) => state.auth);


  return (
    <>
      <Navbar className="color-nav p-1" expand="lg" fixed="top" variant="dark">
        <Container>
          <img src={"/Logos/logo01.png"} alt="logo" 
          // onClick={() => {history.push("/cadastro"); }} 
          style={{ height: 40, cursor: "pointer" }} />
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">

            <Nav>
              <NavLink to="/home" className="ml-2 LinkContent">Principal</NavLink>
              <NavLink to="/cadastro" className="ml-2 LinkContent">Cadastro</NavLink>
            </Nav>


            {/* <Nav>
              <NavDropdown align={"end"} title={authReducer.user.name.split(' ')[0]} id="basic-nav-dropdown" className="user-drop">
                <NavDropdown.Item onClick={() => history.push("/att_senha")}>
                  Alteração de Senha
                </NavDropdown.Item>
                <NavDropdown.Item onClick={() => { auth.logout(); }}>
                  Sair
                </NavDropdown.Item>
              </NavDropdown>
            </Nav> */}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
