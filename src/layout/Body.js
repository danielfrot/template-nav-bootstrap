
export default function Body({children, backgroundColor = '#fff'}) {

  return (
    <section style={{  padding: '0.5em',  marginTop: '3rem',  }}>
      {children}
    </section>
  );
}
