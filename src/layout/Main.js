import React, { useState, useEffect, useRef, memo } from "react";
import Body from "./Body";
import { useDispatch, useSelector } from "react-redux";
// import { useAuth } from "../hooks/useAuth";
import { NavigationBar } from "./NavigationBar";
import { Container } from "react-bootstrap";

export const Main = ({ children, backgroundColor }) => {
  const timerReducer = useSelector((state) => state.timer);
  // const auth = useAuth();

  // const sair = () => {
  //   alert("Sessão expirada!");
  //   auth.logout();
  //   // window.location.reload()
  // };

  // const reset = () => {
  //   timerReducer.reset();
  // };

  // useEffect(() => {
  //   // console.log(timerReducer)
  //   if (timerReducer.expired) {
  //   }
  //   if (timerReducer.min === 59 && timerReducer.seg === 59) {
  //     sair();
  //   }
  // }, [timerReducer.expired, timerReducer.min]);

  return (
    <div style={{backgroundColor: "#d0ddde", height: '100vh'}}>
      <NavigationBar />
      <section style={{ backgroundColor: "#d0ddde", padding: "0.8em"}}>
        <Container
          fluid
          style={{
            backgroundColor: "#f4f4ef",
            borderRadius: "5px",
            border: "1px inset #248FD6",
          }}
        >
          <Body children={children} backgroundColor={backgroundColor} />
        </Container>
      </section>
    </div>
  );
};
