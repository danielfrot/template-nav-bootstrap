

export default function Minimum({ children }) {

  return (<>
    <section style={{
      backgroundColor: '#666',
      height: '100vh',
      overflow: 'auto',
      display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'
    }}>

      <div container direction="row" justifyContent="center" alignItems="center">
      <img style={{transform: "translate(0px, -30%)"}} src="/Logos/logo01.png" width="330" height="70" className="c-sidebar-brand-full" alt="logo"></img>
        <div style={{
          backgroundColor: '#fff', height: 240, width: '23vw', minWidth: '300px',
          padding: '1.0em', borderRadius: '5px',
        }}>

          {children}
        </div>
      </div>

    </section>
  </>)
}