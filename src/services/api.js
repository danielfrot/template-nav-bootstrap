export const base_url = process.env.REACT_APP_API_BASEURL;

// export const headers = {
//     'Accept': 'application/json',
//     'Content-Type': 'application/json',
//     'Authorization': `bearer ${sessionStorage.getItem('token')}`
//   };

export const login_api = `${base_url}/login`;
export const logout_api = `${base_url}/logout`;
export const profile_api = `${base_url}/profile`;
export const refresh_token_api = `${base_url}/refresh_token`;
// export const return_user_api       = `${base_url}/return_user`;
// export const return_data_token_api = `${base_url}/return_data_token`;


export const user_api = `${base_url}/user`;
export const people_api = `${base_url}/people`;
export const people_anexo_api = `${base_url}/anexo`;
export const people_sentence_api = `${base_url}/people_sentence`;
export const people_activities_api = `${base_url}/people_activities`;
export const people_neighbors= `${base_url}/people_neighbors`;
export const social_services= `${base_url}/social_services`;
export const people_assistance_partners= `${base_url}/assistance_partners`;
export const social_programs= `${base_url}/social_programs`;
export const people_social_programs= `${base_url}/people_social_programs`
export const people_social_services= `${base_url}/people_social_services`;

export const people_agenda_api = `${base_url}/people_agenda`;
export const groups_api = `${base_url}/groups`;
export const cities_api = `${base_url}/cities`;
export const medicine_api = `${base_url}/medicine`;
export const diseases_api = `${base_url}/diseases`;
export const disabilities_api = `${base_url}/disabilities`;
export const drugs_api = `${base_url}/drugs`;
export const people_drugs_api = `${base_url}/people_drugs`;
export const people_disabilities_api = `${base_url}/people_disabilities`;
export const people_diseases_api = `${base_url}/people_diseases`;
export const people_medicines_api = `${base_url}/people_medicines`;
export const people_pendencies_api = `${base_url}/people_pendencies`;
export const p_tipo_documentos_api = `${base_url}/p_tipo_documentos`;
export const marital_statuses_api = `${base_url}/marital_statuses`;
export const professional_situations_api = `${base_url}/professional_situations`;
export const occupations_api = `${base_url}/occupations`;
export const races_api = `${base_url}/races`;
export const tasks_participants_api = `${base_url}/tasks_participants`;
export const rooms_api = `${base_url}/rooms`;
export const activities_api = `${base_url}/activities`;
export const tasks_api = `${base_url}/tasks`;
export const employees_api = `${base_url}/employees`;
export const partners_api = `${base_url}/partners`;
export const url_api = `${base_url}/url`;
export const school_type_api = `${base_url}/school_types`;
export const schooling_degrees_api = `${base_url}/schooling_degrees`;
export const trash_destinations_api = `${base_url}/trash_destinations`;
export const waste_destinations_api = `${base_url}/waste_destinations`;
export const water_supplies_api = `${base_url}/water_supplies`;
export const housing_types_api = `${base_url}/housing_types`;
export const housing_situations_api = `${base_url}/housing_situations`;
export const acl_api = `${base_url}/acl`;
export const graphql_api = `${base_url.split("v1")[0]}graphql`;
export const skills = `${base_url}/skills`;
export const kinship_degrees = `${base_url}/kinship_degrees`;
export const people_pains = `${base_url}/people_pains`;
export const body_parts = `${base_url}/body_parts`;
export const hospitals = `${base_url}/hospitals`;
export const dreams = `${base_url}/dreams`;
export const sexual_orientations = `${base_url}/sexual_orientations`;
export const activities_type = `${base_url}/activities_type`;
export const activities_groups = `${base_url}/activity-groups`;

