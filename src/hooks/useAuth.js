import { useCookies } from "react-cookie";
import { login_api, profile_api } from "../services/api";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import axios from "axios";
// import authActions from "../store/actions/authActions";
import { toast } from "react-toastify";
// import ocorrenciasActions from "src/store/actions/ocorrenciasActions";

export function useAuth() {
  const [cookies, setCookie, removeCookie] = useCookies();
  const dispatch = useDispatch();
  const history = useHistory();

  const login_user = (cpf, password) => {
    axios
      .post(login_api, { cpf, password: password })
      .then((res) => {
        setCookie("access_token", res.data.access_token, {
          path: "/",
          maxAge: 60 * 60 * 24,
          // expires: new Date(120)
        });
        profile(res.data.access_token);
        history.push("/ocorrencias");
        toast.success("Logado com sucesso!");
        console.log(res.data)
      })
      .catch((err) => {
        toast.error(err?.response?.data?.error)
      });
  };

  const logout = () => {
    // dispatch(authActions.logout());
    removeCookie("access_token");
    history.push("/autenticacao")
  };

  const profile = (access_token) => {
    if (access_token) {
      axios
        .get(profile_api, {
          headers: { Authorization: `Bearer ${access_token}` },
        })
        .then((r) => {
          console.log(r.data)
          // dispatch(authActions.login(r.data));
          // dispatch(ocorrenciasActions.unidade_plantao_atual_id(r?.data?.userRelation?.lotacoes_lotacoes_fk_usuarioTousers[0]?.unidade_plantao_id))
          // dispatch(ocorrenciasActions.index_ocorrencia_unidade(r?.data?.user?.id, access_token));
          localStorage.setItem("unidade_plantao_id", r?.data?.userRelation?.lotacoes_lotacoes_fk_usuarioTousers[0]?.unidade_plantao_id)
        })
        .catch((err) => {
          // dispatch(authActions.logout());
          removeCookie("access_token");
        });
    } else {
      axios
        .get(profile_api, {
          headers: { Authorization: `Bearer ${cookies.access_token}` },
        })
        .then((response) => {
          // dispatch(authActions.login(response.data));
        })
        .catch((err) => {
          // dispatch(authActions.logout());
          removeCookie("access_token");
        });
    }
  };
  return {
    login_user,
    profile,
    logout,
  };
}
