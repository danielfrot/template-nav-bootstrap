import { useEffect } from 'react';
import { Switch } from 'react-router-dom';
import { RouteWithLayout } from './RouteWithLayout';
import { useDispatch, useSelector } from 'react-redux';
import { Main as MainLayout } from '../layout/Main';
import MinimumLayout from '../layout/Minimum';
import { useHistory, useLocation } from "react-router-dom";
// import { useAuth } from '../hooks/useAuth'
import Home from '../pages/TelaDashboard'
import Login from '../pages/TelaLogin'
import Cadastro from '../pages/TabelaCadastroPessoa'
// import { useTimer } from '../hooks/useTimer';
import timerActions from '../redux/actions/timerActions';
// import RegistroDeAtividades from '../components/RegistroDeAtividades'
import Cadastrar from '../pages/Cadastrar'


const Index = () => {
  // const auth = useAuth()
  const dispatch = useDispatch()
  const authReducer = useSelector(state => state.auth);
  const history = useHistory();
  const location = useLocation();
  // const timer = useTimer()

  useEffect(() => {
    if (sessionStorage.getItem('access_token') && !authReducer.isLogged) {
      // auth.refreshToken()
    }
    // if (true) {
    //   if (location.pathname === '/' || location.pathname === '/login') {
    //     history.push('/cadastro')
    //   }
    //   if (location.pathname !== '/login' && !sessionStorage.getItem('access_token')) {
    //     history.push('/login')
    //   }
    // } else {
    //   if (location.pathname !== '/login' && !sessionStorage.getItem('access_token')) {
    //     history.push('/login')
    //   }
    // }
  }, [])

  // useEffect(() => {
  //   dispatch(timerActions.addFn_timer(timer.resetTimer))
  //   timer.startTimer()
  // }, [])

  return (
    <Switch>
      {/* {false ? */}
        <>
          <RouteWithLayout exact component={Home} layout={MainLayout} backgroundColor={'#d0ddde'} path="/home" />
          <RouteWithLayout exact component={Cadastro} layout={MainLayout} path="/cadastro" />
          <RouteWithLayout exact component={Cadastrar} layout={MainLayout} path="/Cadastrar" />
          {/* <RouteWithLayout exact component={RegistroDeAtividades} layout={MainLayout} path="/registro_atividades/cadastro/:id" /> */}
        <RouteWithLayout component={Login} layout={MinimumLayout} path="/login" style={{ backgroundColor: 'red' }} />
        </>
        {/* : */}
      {/* } */}




    </Switch>
  )
};

export default Index;
