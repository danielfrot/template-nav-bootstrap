import { Route } from 'react-router-dom';

export const RouteWithLayout = props => {
  const { layout: Layout, component: Component, backgroundColor, ...rest } = props;
  
  return (
    <Route
      {...rest}
      render={matchProps => (
        <Layout backgroundColor={backgroundColor} >
          <Component {...matchProps} />
        </Layout>
      )}
    />
  );
};
